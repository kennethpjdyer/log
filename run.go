package log

import (
	"fmt"
	"github.com/spf13/viper"
	"time"
)

// Run function provides a simple tool for testing logging fucntionality from
// the command line.
func Run(args []string) {

	// Initialize Controller
	Init("LOGGER-TEST", viper.GetBool("verbosity"), viper.GetBool("debug"))

	for _, name := range args {
		New(name, true)
	}

	for i := 0; i <= len(args); i++ {
		Trace(i, "Testing trace logging")
		Debug(i, "Testing debug logging")
		Info(i, "Testing informational logging")
		Warn(i, "Testing warning logging")
		Error(i, "Testing error logging")
	}

	fmt.Println("Now sleeping to test C-c behavior")
	time.Sleep(time.Second * 10)

	Close(0)
}
