package log

import (
	"fmt"
	"io"
	"io/ioutil"
	stdlog "log"
)

// Logger struct stores and manages a set of six Logger pointers, initialized
// from the standard Go `log` module.
//
// Each logger is initializes with a specific label indicating the logging
// level, which is used in sending messages to stderr.
type Logger struct {
	name string
	lgrs map[int]*stdlog.Logger
}

func initLogger(writer io.Writer, label string) *stdlog.Logger {
	return stdlog.New(writer, label, 0)
}

func initNull() *stdlog.Logger {
	return stdlog.New(io.Discard, "", 0)
}

func discard() *stdlog.Logger {
	return initLogger(ioutil.Discard, "")
}

func getLogger(name string, debug bool) *Logger {
	l := &Logger{
		name: fmt.Sprintf(":%s", name),
		lgrs: make(map[int]*stdlog.Logger),
	}
	l.setLabels(debug)
	return l
}

// New function takes a string and a debug boolean value and uses them to
// initializes a new Logger pointer in the controller registry.
func New(name string, debug bool) int {
	l := getLogger(name, debug)
	key := len(Control.loggers)
	Control.loggers[key] = l
	return key
}
