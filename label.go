package log

import (
	"fmt"
	"github.com/spf13/viper"
	stdlog "log"
	"os"
)

const (
	traceLevel int = iota
	debugLevel
	infoLevel
	warnLevel
	errLevel
	fatalLevel
)

func getLabelName(level int) (string, string) {
	switch level {
	case traceLevel:
		return "TRACE", ""
	case debugLevel:
		return "DEBUG", Cyan
	case infoLevel:
		return "INFO", Green
	case warnLevel:
		return "WARN", Yellow
	case errLevel:
		return "ERROR", Red
	case fatalLevel:
		return "FATAL", BoldMagenta
	default:
		return "LOG", ""
	}
}

func formatLabel(labelName, name, color string) string {
	return fmt.Sprintf("[ %s%s%s%s ]: ", color, labelName, name, Reset)
}

func (l *Logger) getLogger(level int, quiet, verbose, debug bool) *stdlog.Logger {
	switch level {
	case traceLevel:
		if debug && verbose {
			return initLogger(os.Stderr, formatLabel("TRACE", l.name, ""))
		}
	case debugLevel:
		if debug {
			return initLogger(os.Stderr, formatLabel("DEBUG", l.name, Cyan))
		}
	case infoLevel:
		if verbose {
			return initLogger(os.Stderr, formatLabel("INFO", l.name, Green))
		}
	case warnLevel:
		if !quiet {
			return initLogger(os.Stderr, formatLabel("WARN", l.name, Yellow))
		}
	case errLevel:
		return initLogger(os.Stderr, formatLabel("ERROR", l.name, Red))
	case fatalLevel:
		return initLogger(os.Stderr, formatLabel("FATAL", l.name, Red))
	}
	return initNull()
}

func (l *Logger) setLabels(debug bool) {
	quiet := viper.GetBool("quiet")
	verbose := viper.GetBool("verbosity")
	for level := traceLevel; level <= fatalLevel; level++ {
		l.lgrs[level] = l.getLogger(level, quiet, verbose, debug)
	}
}
