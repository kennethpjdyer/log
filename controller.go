package log

import (
	"github.com/spf13/viper"
	"os"
	"runtime"
	"sync"
	"time"
)

// Control is controller pointer, which provides the user with an interface
// to manage the logging module through external resources.
var Control *controller

type controller struct {
	name    string
	start   time.Time
	loggers map[int]*Logger
	msgs    chan *message
	wg      sync.WaitGroup
}

func newController(name string) *controller {
	var wg sync.WaitGroup
	c := &controller{
		name:    name,
		start:   time.Now(),
		loggers: make(map[int]*Logger),
		msgs:    make(chan *message, (viper.GetInt("log.buffer")+1)*runtime.NumCPU()),
		wg:      wg,
	}
	c.wg.Add(1)

	return c
}

// Init takes a string variable and initializes the logging module controller.
//
// In addition to the basic initialization, it also sets up a default logger,
// enables a listener to catch Ctrl-c calls and allow the loggers to finish
// printing messages in the channel, and starts the report worker, which monitors
// the logging channel for new logging messages.
func Init(name string, verbose, debug bool) {
	viper.Set("verbosity", verbose)
	Control = newController(name)
	New(name, debug)
	Control.listen()
	go report()
}

func (c *controller) exit(code int) {
	close(c.msgs)
	c.wg.Wait()
	os.Exit(code)
}

func exitMsg(code int) {
	if code == 0 {
		send(0, infoLevel, "Exit Success")
	}
}

// Close function is used to shut down the logging module and application.  If
// this is a graceful shutdown, it also prints a simple success message noting
// the exit.
func Close(code int) {
	exitMsg(code)
	Control.exit(code)
}
