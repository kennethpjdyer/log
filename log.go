// Package log provdes a controller and a series of functions for managing
// logging messages printed to stdout.  It is only slightly more sophisticated
// than the standard Go `log` package in that it provides support for labels
// and logging levels.
//
// Note: This package overrides the standard C-c behavior to ensure that the
// logging channel is emptied before closing.
package log
