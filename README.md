# Log

The log package requires standard functions for initializing the standard
Go `log` module for Avocet Tools.

## Installation

```sh
$ go get gitlab.com/kennethpjdyer/log
```

## Usage

```go
import "gitlab.com/kennethpjdyer/log"

func main(){

   log.Init(
      "MODULE-NAME", // String indicating the name of the application
      true,          // Boolean indicating whether to use verbose output 
      false,         // Boolean indicating whether to use debugging output
   )

   // To send a message to the default logger
   log.Warn(
      0,                  // Default Logger Key 
      "Warning Message",  // Warning Message
    )

    // To create a new logger
    logKey := log.New("SUBMODULE", false)
    log.Warn(logKey, "Submodule warning")
}
```

## Reference

### Logging Levels

| Level   | Required Options |
|---------|------------------|
| `TRACE` | Verbose Debug    |
| `DEBUG` | Debug            |
| `INFO`  | Verbose          |
| `WARN`  | Not-quiet        |
| `ERROR` |                  |
| `FATAL` |                  |

> **Note**: Messages sent at the `FATAL` level trigger an `EXIT_FAILURE`. 

