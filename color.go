package log

// Color constants
const (
	Reset       = "\033[0m"
	Bold        = "\033[1m"
	Cyan        = "\033[0;36m"
	BoldCyan    = "\033[1;36m"
	Blue        = "\033[0;34m"
	BoldBlue    = "\033[1;34m"
	Green       = "\033[0;32m"
	BoldGreen   = "\033[1;32m"
	Yellow      = "\033[0;33m"
	BoldYellow  = "\033[1;33m"
	Purple      = "\033[0;35m"
	BoldPurple  = "\033[1;35m"
	Magenta     = "\033[0;33m"
	BoldMagenta = "\033[1;35m"
	Red         = "\033[0;31m"
	BoldRed     = "\033[1;31m"
)
