package main

import (
	// Standard Library
	"fmt"
	"os"
	"strings"

	// External Imports
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	// Local Imports
	"gitlab.com/kennethpjdyer/log"
)

const ver string = "0.1.0"

/*
reportVersion function takes a boolean argument to indicate verobsity, and then
prints to stdout information on the current version and built of Avocet Tools.
*/
func reportVersion(verbose bool) {
	var content []string
	if verbose {
		content = []string{
			"logger-test - A log message testing application",
			"Kenneth P. J. Dyer <kenneth@avoceteditors.com>",
			"Avocet Editorial Consulting",
			fmt.Sprintf("Version: v%s", ver),
		}
	} else {
		content = []string{fmt.Sprintf("dion - version %s\n", ver)}
	}
	fmt.Println(strings.Join(content, "\n  "))
	os.Exit(0)
}

var (
	cmd = &cobra.Command{
		Use:   "logger-test",
		Short: "Simple test for logger operations",
		Run: func(cmd *cobra.Command, args []string) {
			log.Run(args)
		},
	}
)

func init() {

	// Debug
	cmd.PersistentFlags().BoolP(
		"debug", "D",
		viper.GetBool("debug"),
		"Enables debugging output for logs")
	viper.BindPFlag("debug", cmd.PersistentFlags().Lookup("debug"))

	// Verbosity Flag
	cmd.PersistentFlags().BoolP(
		"verbose", "v",
		viper.GetBool("verbosity"),
		"Enables verbose output")
	viper.BindPFlag("verbosity", cmd.PersistentFlags().Lookup("verbose"))
}

func main() {
	cmd.Execute()
}
