package log

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"
)

type message struct {
	key     int
	level   int
	message string
}

func send(key, level int, msg string) {
	Control.msgs <- &message{
		key:     key,
		level:   level,
		message: msg,
	}
}

func report() {
	for msg := range Control.msgs {
		logger, ok := Control.loggers[msg.key]
		if !ok {
			Control.loggers[0].Log(fatalLevel, fmt.Sprint("Invalid Logger Key: ", msg.key))
			os.Exit(1)
		}
		logger.Log(msg.level, msg.message)
	}
	Control.wg.Done()
}

func (c *controller) listen() {
	ctrl := make(chan os.Signal, 1)
	signal.Notify(ctrl, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-ctrl
		send(0, fatalLevel, "Received keyboard interrupt")
		Close(1)
	}()
}

// Log method is the standard reporting function for the Logger struct. This
// function takes an integer level and a string message.  It uses the level to
// identify the correct logger to use in calling the a log.Logger.Println
// method
func (l *Logger) Log(level int, msg string) {
	l.lgrs[level].Println(msg)
}

// Trace function logs a trace message.
func Trace(key int, msg ...any) {
	send(key, traceLevel, fmt.Sprint(msg...))
}

// Tracef function logs a formated trace message.
func Tracef(key int, patt string, msg ...any) {
	send(key, traceLevel, fmt.Sprintf(patt, msg...))
}

// Debug function logs a debug message
func Debug(key int, msg ...any) {
	send(key, debugLevel, fmt.Sprint(msg...))
}

// Debugf function logs a formatted debug message
func Debugf(key int, patt string, msg ...any) {
	send(key, debugLevel, fmt.Sprintf(patt, msg...))
}

// Info function logs an informational message
func Info(key int, msg ...any) {
	send(key, infoLevel, fmt.Sprint(msg...))
}

// Infof function logs a formatted informational message
func Infof(key int, patt string, msg ...any) {
	send(key, infoLevel, fmt.Sprintf(patt, msg...))
}

// Warn function logs a warning message
func Warn(key int, msg ...any) {
	send(key, warnLevel, fmt.Sprint(msg...))
}

// Warnf function logs a formatted warning message
func Warnf(key int, patt string, msg ...any) {
	send(key, warnLevel, fmt.Sprintf(patt, msg...))
}

// Error function logs an error message
func Error(key int, msg ...any) {
	send(key, errLevel, fmt.Sprint(msg...))
}

// Errorf function logs a formatted error message
func Errorf(key int, patt string, msg ...any) {
	send(key, errLevel, fmt.Sprintf(patt, msg...))
}

// Fatal function logs a fatal message and exits failure
func Fatal(key int, msg ...any) {
	send(key, fatalLevel, fmt.Sprint(msg...))
	Close(1)
}

// Fatalf function logs a formattd fatal message and exits failure
func Fatalf(key int, patt string, msg ...any) {
	send(key, fatalLevel, fmt.Sprintf(patt, msg...))
	Close(1)
}
