
LOG="gitlab.com/kennethpjdyer/log"
all: lint fmt build

build:
	@echo "Building   $(LOG)"
	@go build ./cmd/logger-test

lint:
	@echo "Linting    $(LOG)"
	@golint

fmt:
	@echo "Formatting $(LOG)"
	@gofmt -w .
